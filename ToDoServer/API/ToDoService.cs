﻿using Grpc.Core;
using Todolist;
using ToDoServer.Data.Repository;

namespace ToDoServer.API;

public class ToDoService : Todolist.ToDoService.ToDoServiceBase
{
    private readonly IToDoRepository _repository;

    public ToDoService(IToDoRepository repository)
    {
        _repository = repository;
    }
    
    public override async Task<ResultResponse> EditToDo(ToDoModel request, ServerCallContext context)
    {
        await _repository.EditToDoAsync(request.Id, request.Title, request.Desc);
        return new ResultResponse() { Succeded = true };
    }

    public override async Task<ResultResponse> AddNewToDo(ToDoModel request, ServerCallContext context)
    {
        await _repository.AddNewToDoAsync(request.Id, request.Title, request.Desc);
        return new ResultResponse() { Succeded = true };
    }

    public override async Task<GetAllToDosResponse> GetAllToDos(EmptyRequest request, ServerCallContext context)
    {
        var databaseData = await _repository.GetAllAsync();
        
        GetAllToDosResponse response = new GetAllToDosResponse();
        response.AllToDos.AddRange(databaseData.Select(x => new ToDoModel()
        {
            Desc = x.Desc,
            Id = x.Id,
            Title = x.Title,
        }).ToList());

        return response;
    }
}