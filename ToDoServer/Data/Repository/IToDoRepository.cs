﻿using ToDoServer.Data.Models;

namespace ToDoServer.Data.Repository;

public interface IToDoRepository
{
    Task AddNewToDoAsync(int id, string title, string desc);
    Task EditToDoAsync(int id, string title, string desc);
    Task<List<ToDoDatabaseModel>> GetAllAsync();
}