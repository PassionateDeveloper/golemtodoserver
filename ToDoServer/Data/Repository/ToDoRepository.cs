﻿using Microsoft.EntityFrameworkCore;
using ToDoServer.Data.Context;
using ToDoServer.Data.Models;

namespace ToDoServer.Data.Repository;

public class ToDoRepository : IToDoRepository
{
    private readonly ToDoContext _toDoContext;

    public ToDoRepository(ToDoContext toDoContext)
    {
        _toDoContext = toDoContext;
    }
    
    public async Task AddNewToDoAsync(int id, string title, string desc)
    {
        _toDoContext.ToDos.Add(new ToDoDatabaseModel()
        {
            Id = id,
            Title = title,
            Desc = desc
        });

        await _toDoContext.SaveChangesAsync();
    }

    public async Task EditToDoAsync(int id, string title, string desc)
    {
        var item = await _toDoContext.ToDos.SingleOrDefaultAsync(x => x.Id == id);
        if (item == null)
        {
            throw new Exception("ToDo Item not found");
        }

        item.Desc = desc;
        item.Title = title;

        await _toDoContext.SaveChangesAsync();
    }

    public async Task<List<ToDoDatabaseModel>> GetAllAsync()
    {
        return await _toDoContext.ToDos.ToListAsync();
    }
}