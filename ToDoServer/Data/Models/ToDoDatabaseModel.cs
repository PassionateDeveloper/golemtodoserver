﻿namespace ToDoServer.Data.Models;

public class ToDoDatabaseModel
{
    public int Id { get; set; }
    
    public string Title { get; set; }
    
    public string Desc { get; set; }
}