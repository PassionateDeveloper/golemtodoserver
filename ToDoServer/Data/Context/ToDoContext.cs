﻿using Microsoft.EntityFrameworkCore;
using ToDoServer.Data.Models;

namespace ToDoServer.Data.Context;

public class ToDoContext : DbContext
{
    public ToDoContext(DbContextOptions<ToDoContext> options)
        : base(options)
    {
    }
 
    public DbSet<ToDoDatabaseModel> ToDos { get; set; }
}