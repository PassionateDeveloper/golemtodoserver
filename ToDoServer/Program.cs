using Microsoft.EntityFrameworkCore;
using ToDoServer.API;
using ToDoServer.Data.Context;
using ToDoServer.Data.Repository;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddGrpc();
builder.Services.AddScoped<IToDoRepository, ToDoRepository>();
builder.Services.AddDbContext<ToDoContext>(x => x.UseInMemoryDatabase("ToDoDb"));
var app = builder.Build();

app.MapGet("/", () => "Hello World!");
app.MapGrpcService<ToDoService>();
app.Run();